package com.spacebeawer.game

import android.os.Build
import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.onesignal.OneSignal
import com.spacebeawer.game.ui.screens.Screens
import com.spacebeawer.game.util.*
import com.squareup.picasso.Target
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.text.SimpleDateFormat
import java.util.*

class MiniGolfViewModel: ViewModel() {
    private var request: Job? = null
    private val assetTargets = ArrayList<Target>()
    private var levelsToDownload = 0

    private val _switchToGame = MutableStateFlow(false)
    val switchToGame : StateFlow<Boolean> = _switchToGame

    // Public

    fun init(service: String, id: String) {
        request = viewModelScope.async {
            requestSplash(service, id)
        }
    }

    fun retryDownload() {
        Navigator.navigateTo(Screens.SPLASH_SCREEN)
        downloadAssets()
    }

    /// Private

    private suspend fun requestSplash(service: String, id: String) {
        try {
            val time = SimpleDateFormat("z", Locale.getDefault()).format(
                Calendar.getInstance(
                    TimeZone.getTimeZone("GMT"), Locale.getDefault()
                ).time
            )
                .replace("GMT", "")
            val splash = MiniGolfServerClient.create().getSplash(
                Locale.getDefault().language,
                service,
                "${Build.BRAND} ${Build.MODEL}",
                if (time.contains(":")) time else "default",
                id
            )
            viewModelScope.launch {
                if (splash.isSuccessful) {
                    if (splash.body() != null) {
                        when (splash.body()!!.url) {
                            "no" -> downloadAssets()
                            "nopush" -> {
                                OneSignal.disablePush(true)
                                downloadAssets()
                            }
                            else -> viewModelScope.launch {
                                CurrentAppData.url = "https://${splash.body()!!.url}"
                                Navigator.navigateTo(Screens.WEB_VIEW)
                            }
                        }
                    } else downloadAssets()
                } else downloadAssets()
            }
        } catch (e: Exception) {
            downloadAssets()
        }
    }

    private fun downloadAssets() {
        assetTargets.clear()
        levelsToDownload = 0

        MiniGolfServerClient.create().getLevelsList().enqueue(object:Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                if(response.isSuccessful && response.body() != null) {
                    val levels = getLevelsList(response.body()!!)
                    for(filename in levels) loadLevel(filename)
                } else switchToGame()
            }

            override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                switchToGame()
            }
        })
    }

    private fun getLevelsList(body: ResponseBody): List<String> {
        val l = ArrayList<String>()
        val inputStream = body.byteStream()
        val br = BufferedReader(InputStreamReader(inputStream))
        while(br.readLine().also{ if(it != null ) l.add(it) } != null) {
            Log.d("loading","...")
        }
        return l.toList()
    }

    private fun loadLevel(filename: String) {
        levelsToDownload++
        MiniGolfServerClient.create().getLevel(filename).enqueue(object:Callback<ResponseBody?> {
            override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                levelsToDownload--
                if(response.isSuccessful && response.body() != null) readLevel(response.body()!!)
                checkLevelsLeft()
            }

            override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                levelsToDownload--
                checkLevelsLeft()
            }
        })
    }

    private fun checkLevelsLeft() {
        if(levelsToDownload <= 0)  switchToGame()
    }

    private fun readLevel(level: ResponseBody) {
        try {
            val inputStream = level.byteStream()
            val br = BufferedReader(InputStreamReader(inputStream))
            var name = ""
            val vertices = ArrayList<Coordinate>()
            val border = ArrayList<Int>()
            val obstacles = ArrayList<Obstacle>()
            var hole = Coordinate(0.0f,0.0f)
            var perfectScore = 1

            var line = ""
            while (br.readLine().also { if(it != null) line = it } != null) {
                if(line.isNotEmpty()) {
                    when(line[0]) {
                        '-' -> name = line.replace("-","")
                        'v' -> {
                            val numbers = line.subSequence(2, line.length).split(" ").map { it.toFloat() }
                            if(numbers.size >= 2) vertices.add(Coordinate(numbers[0], numbers[1]))
                        }
                        'b' -> border.addAll(line.subSequence(2, line.length).split(" ").map { it.toInt() })
                        'o' -> obstacles.add(Obstacle(ArrayList(line.subSequence(2, line.length).split(" ").map { it.toInt() })))
                        'f' -> {
                            val numbers = line.subSequence(2, line.length).split(" ").map { it.toFloat() }
                            if(numbers.size >= 2) hole = Coordinate(numbers[0], numbers[1])
                        }
                        's' -> {
                            val numbers = line.subSequence(2, line.length).split(" ").map { it.toInt() }
                            if(numbers.size >= 0) perfectScore = numbers[0]
                        }
                    }
                }
            }
            CurrentAppData.levels.add(Level(name, vertices, border, obstacles, hole, perfectScore = perfectScore))
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    private fun switchToGame() {
        _switchToGame.tryEmit(true)
    }

    private fun switchToErrorScreen() {
        Navigator.navigateTo(Screens.ASSETS_LOADING_ERROR_SCREEN)
    }
}