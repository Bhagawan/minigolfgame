package com.spacebeawer.game.util

import androidx.activity.compose.BackHandler
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.spacebeawer.game.ui.screens.ErrorScreen
import com.spacebeawer.game.ui.screens.Screens
import com.spacebeawer.game.ui.screens.SplashScreen
import com.spacebeawer.game.ui.screens.WebViewScreen
import im.delight.android.webview.AdvancedWebView
import kotlin.system.exitProcess

@Composable
fun NavigationComponent(navController: NavHostController, webView: AdvancedWebView, errorScreenAction : () -> Unit) {
    NavHost(
        navController = navController,
        startDestination = Screens.SPLASH_SCREEN.label
    ) {
        composable(Screens.SPLASH_SCREEN.label) {
            SplashScreen()
        }
        composable(Screens.ASSETS_LOADING_ERROR_SCREEN.label) {
            BackHandler(true) {}
            ErrorScreen(errorScreenAction)
        }
        composable(Screens.WEB_VIEW.label) {
            BackHandler(true) {
                if (webView.onBackPressed()) exitProcess(0)
            }
            WebViewScreen(webView, remember { CurrentAppData.url })
        }
    }
    val currentScreen by Navigator.navigationFlow.collectAsState(initial = Screens.SPLASH_SCREEN)
    navController.navigate(currentScreen.label)

    navController.enableOnBackPressed(true)
}