package com.spacebeawer.game.util

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface MiniGolfServerClient {

    @FormUrlEncoded
    @POST(UrlSplash)
    suspend fun getSplash(@Field("locale") locale: String
                  , @Field("simLanguage") simLanguage: String
                  , @Field("model") model: String
                  , @Field("timezone") timezone: String
                  , @Field("id") id: String ): Response<MiniGolfSplashResponse>

    @GET(UrlLevels)
    fun getLevelsList(): Call<ResponseBody>

    @GET("$UrlLevels_folder/{filename}.txt")
    fun getLevel(@Path(value = "filename", encoded = false) filename: String): Call<ResponseBody?>

    companion object {
        fun create() : MiniGolfServerClient {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://195.201.125.8/")
                .build()
            return retrofit.create(MiniGolfServerClient::class.java)
        }
    }

}