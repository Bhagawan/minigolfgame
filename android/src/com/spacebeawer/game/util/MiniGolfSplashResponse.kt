package com.spacebeawer.game.util

import androidx.annotation.Keep

@Keep
data class MiniGolfSplashResponse(val url : String)