package com.spacebeawer.game.util

import com.spacebeawer.game.ui.screens.Screens
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

object Navigator {
    private val mNavigationFlow = MutableStateFlow(Screens.SPLASH_SCREEN)
    val navigationFlow = mNavigationFlow.asStateFlow()

    fun navigateTo(targetScreens: Screens) {
        mNavigationFlow.tryEmit(targetScreens)
    }
}