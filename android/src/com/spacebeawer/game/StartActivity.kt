package com.spacebeawer.game

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.telephony.TelephonyManager
import android.view.View
import android.webkit.WebChromeClient
import android.widget.FrameLayout
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import androidx.navigation.compose.rememberNavController
import com.spacebeawer.game.ui.theme.MiniGolfTheme
import com.spacebeawer.game.util.NavigationComponent
import im.delight.android.webview.AdvancedWebView
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import java.util.*

class StartActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val viewModel = ViewModelProvider(this)[MiniGolfViewModel::class.java]
        viewModel.switchToGame.onEach {
            if(it) switchToGame()
        }.launchIn(viewModel.viewModelScope)
        if(savedInstanceState == null) viewModel.init((getSystemService(TELEPHONY_SERVICE) as TelephonyManager).simCountryIso, getId())
        setContent {
            val webView = remember { createWebView() }
            val navController = rememberNavController()
            MiniGolfTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    NavigationComponent(navController, webView, viewModel::retryDownload)
                }
            }
        }
    }

    private fun switchToGame() {
        startActivity(Intent(this, AndroidLauncher::class.java))
        finish()
    }

    private fun createWebView() : AdvancedWebView {
        return AdvancedWebView(this).apply {
            setMixedContentAllowed(true)
            webChromeClient = object: WebChromeClient() {
                private var h = 0
                private var uiVisibility = 0
                private var d: View? = null
                private var a: CustomViewCallback? = null
                override fun getDefaultVideoPoster(): Bitmap? = if(d == null) null else BitmapFactory.decodeResource(resources, R.drawable.ic_baseline_video)
                override fun onHideCustomView() {
                    (window.decorView as FrameLayout).removeView(d)
                    d = null
                    window.decorView.systemUiVisibility = uiVisibility
                    requestedOrientation = h
                    a!!.onCustomViewHidden()
                    a = null
                }

                override fun onShowCustomView(view: View?, callback: CustomViewCallback?) {
                    if(d != null) {
                        onHideCustomView()
                        return
                    }
                    d = view
                    uiVisibility = window.decorView.systemUiVisibility
                    h = requestedOrientation
                    a = callback
                    (window.decorView as FrameLayout).addView(d, FrameLayout.LayoutParams(
                        FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT))
                    WindowInsetsControllerCompat(window, window.decorView).let { controller ->
                        controller.hide(WindowInsetsCompat.Type.systemBars())
                        controller.systemBarsBehavior = WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
                    }
                }
            }
            settings.userAgentString = settings.userAgentString.replace("; wv", "")
        }
    }

    private fun getId() : String {
        val shP = getSharedPreferences("FootballStrategyApp", Context.MODE_PRIVATE)
        var id = shP.getString("id", "default") ?: "default"
        if(id == "default") {
            id = UUID.randomUUID().toString()
            shP.edit().putString("id", id).apply()
        }
        return id
    }
}