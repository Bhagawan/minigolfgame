package com.spacebeawer.game

import android.os.Bundle
import com.badlogic.gdx.backends.android.AndroidApplication
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration
import com.spacebeawer.game.util.CurrentAppData

class AndroidLauncher: AndroidApplication() {

	override fun onCreate (savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		val config = AndroidApplicationConfiguration()
		initialize(GolfGame(CurrentAppData.levels), config)
	}
}
