package com.spacebeawer.game.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)


val Orange = Color(0xFF9C7341)
val Red = Color(0xFF793A34)
val Grey = Color(0x80474747)
val Green = Color(0xFF3F612A)
