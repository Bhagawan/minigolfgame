package com.spacebeawer.game.ui.screens

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color.Companion.Yellow
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.unit.Dp
import com.spacebeawer.game.util.UrlLogo
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target

@Composable
fun SplashScreen() {
    var image by remember { mutableStateOf<ImageBitmap?>(null) }
    LaunchedEffect("logo") {
        val picasso = Picasso.get()

        val target = object : Target {
            override fun onPrepareLoad(placeHolderDrawable: Drawable?) { }
            override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) { }
            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                image = bitmap?.asImageBitmap()
            }
        }

        picasso.load(UrlLogo).into(target)
    }
    Column(
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center) {
        image?.let {
            Image(it, contentDescription = null)
        }

        CircularProgressIndicator( color = Yellow, strokeWidth = Dp(5.0f),
            modifier = Modifier
                .padding(Dp(20.0f))
                .size(Dp(100.0f)))
    }
}