package com.spacebeawer.game.assets

// For Assets credit to Penzilla

import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.scenes.scene2d.ui.Skin

object Assets {
    var assetManager = AssetManager()

    fun init() {
        assetManager = AssetManager()
        assetManager.load("skin/MiniGolf.atlas", TextureAtlas::class.java)
        assetManager.load("skin/MiniGolf.json", Skin::class.java)

        while (!assetManager.update()) { println(" loading assets ") }
    }

    fun dispose() {
        assetManager.dispose()
    }

    fun atlas() : TextureAtlas = assetManager.get("skin/MiniGolf.atlas", TextureAtlas::class.java)

    fun skin() : Skin = assetManager.get("skin/MiniGolf.json", Skin::class.java)

}