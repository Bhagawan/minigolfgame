package com.spacebeawer.game.screens

import com.badlogic.gdx.*
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.ui.TextButton
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.scenes.scene2d.utils.TiledDrawable
import com.badlogic.gdx.utils.ScreenUtils
import com.spacebeawer.game.GolfGame
import com.spacebeawer.game.assets.Assets
import com.spacebeawer.game.util.SavedData

class MenuScreen(private val game: Game): ScreenAdapter() {
    private val stage = Stage()

    init {
        createMenu()
    }

    override fun show() {
        val inputAdapter = object : InputAdapter() {

            override fun keyDown(keycode: Int): Boolean = when(keycode) {
                Input.Keys.TAB -> {
                    val levels = SavedData.getLevels()
                    if(levels.isNotEmpty()) game.screen = GameScreen(game, levels.random())
                    true
                }
                else -> false
            }
        }
        val inputMultiplexer = InputMultiplexer()
        inputMultiplexer.addProcessor(inputAdapter)
        inputMultiplexer.addProcessor(stage)
        Gdx.input.inputProcessor = inputMultiplexer
    }

    override fun render(delta: Float) {
        ScreenUtils.clear(0.0f, 0.0f, 0.0f, 1.0f)
        stage.act(Gdx.graphics.deltaTime.coerceAtMost(GolfGame.PERFECT_DT))
        stage.draw()
    }

    override fun resize(width: Int, height: Int) {
        stage.viewport.update(width, height)
    }

    override fun hide() {
        Gdx.input.inputProcessor = null
        stage.clear()
    }

    override fun dispose() {
        stage.dispose()
    }

    private fun createMenu() {
        val skin = Assets.skin()
        val table = Table()
        table.setFillParent(true)
        val back = TiledDrawable(Assets.skin().getSprite("grass"))
        table.background = back
        stage.addActor(table)

        val btnWidth = Gdx.graphics.width / 3.0f

        val newGameButton = TextButton("Новая Игра", skin,"menuStart")
        newGameButton.addListener(object : ClickListener() {
            override fun touchUp(
                event: InputEvent?,
                x: Float,
                y: Float,
                pointer: Int,
                button: Int
            ) {
                game.screen = LevelsScreen(game)
            }
        })
        table.add(newGameButton).size(btnWidth, 100f).expandY().bottom()
        table.row()

        val exitButton = TextButton("Выход",skin,"menuExit")
        exitButton.addListener( object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                Gdx.app.exit()
            }
        })
        table.add(exitButton).size(btnWidth, 100f).expandY().bottom().padBottom(50.0f)
        table.row()
    }
}
