package com.spacebeawer.game.screens

import com.badlogic.ashley.core.Engine
import com.badlogic.gdx.*
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer
import com.badlogic.gdx.physics.box2d.World
import com.badlogic.gdx.utils.ScreenUtils
import com.spacebeawer.game.systems.*
import com.spacebeawer.game.util.Level

class GameScreen(private val game: Game, level: Level): ScreenAdapter() {
    private var batch = SpriteBatch()
    private val camera = OrthographicCamera()
    private val engine = Engine()
    private val world = World(Vector2(0.0f, 0.0f), true)
    private val debugRenderer = Box2DDebugRenderer()

    private var frameCounter = 0.0f
    private var physicsCamera = false

    private val controlSystem = ControlSystem(world, camera, level, 1)
    private val uiSystem = UISystem(controlSystem, 110).apply {
        setInterface(object : UISystem.UIInterface {
            override fun exit() {
                game.screen = MenuScreen(game)
            }

            override fun nextLevel(level: Level) {
                game.screen = GameScreen(game, level)
            }

        })
    }

    init {
        camera.setToOrtho(false)
        camera.translate( -Gdx.graphics.width / 2.0f, - Gdx.graphics.height / 2.0f)
        engine.addSystem(controlSystem)
        engine.addSystem(BackgroundSystem(camera, 2))
        engine.addSystem(BodyTextureSystem(world, 99))
        engine.addSystem(RenderSystem(batch, camera,100))
        engine.addSystem(uiSystem)
    }

    override fun show() {
        Gdx.input.setCatchKey(Input.Keys.BACK, true)
        val inputAdapter  = object : InputAdapter() {

            override fun keyDown(keycode: Int): Boolean = when(keycode) {
                Input.Keys.TAB -> {
                    game.screen = MenuScreen(game)
                    true
                }
                Input.Keys.BACK -> {
                    game.screen = MenuScreen(game)
                    true
                }
                Input.Keys.W -> {
                    camera.translate(0.0f, 10.0f)
                    true
                }
                Input.Keys.S -> {
                    camera.translate(0.0f, -10.0f)
                    true
                }
                Input.Keys.A -> {
                    camera.translate(-10.0f, 0.0f)
                    true
                }
                Input.Keys.D -> {
                    camera.translate(10.0f, 0.0f)
                    true
                }
                Input.Keys.SPACE -> {
                    if (!physicsCamera) {
                        camera.setToOrtho(false, 20.0f,11.0f)
                        camera.translate( -10.0f, -5.0f)
                        batch.projectionMatrix = camera.combined
                        physicsCamera = true
                    } else {
                        camera.setToOrtho(false, Gdx.graphics.width.toFloat(), Gdx.graphics.height.toFloat())
                        camera.translate( -Gdx.graphics.width / 2.0f, - Gdx.graphics.height / 2.0f)
                        batch.projectionMatrix = camera.combined
                        physicsCamera = false
                    }
                    true
                }
                else -> false
            }
        }
        val inputMultiplexer = InputMultiplexer()
        inputMultiplexer.addProcessor(uiSystem.getStage())
        inputMultiplexer.addProcessor(controlSystem.getInputAdapter())
        inputMultiplexer.addProcessor(inputAdapter)
        Gdx.input.inputProcessor = inputMultiplexer
    }

    override fun render(delta: Float) {
        ScreenUtils.clear(0f, 0f, 0f, 1f)
        batch.projectionMatrix = camera.combined
        camera.update()

        val frameTime = delta.coerceAtMost(0.25f)
        frameCounter += frameTime
        while (frameCounter >= 1.0f/60) {
            world.step(1.0f/60, 6, 6)
            frameCounter -= 1.0f/60
        }
        if(physicsCamera) debugRenderer.render(world, camera.combined)

        if(!physicsCamera) engine.update(delta)
    }

    override fun hide() {
        Gdx.input.inputProcessor = null
    }

    override fun dispose() {
        batch.dispose()
    }
}