package com.spacebeawer.game.screens

import com.badlogic.gdx.Game
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.ScreenAdapter
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable
import com.badlogic.gdx.scenes.scene2d.utils.TiledDrawable
import com.badlogic.gdx.utils.Align
import com.badlogic.gdx.utils.ScreenUtils
import com.spacebeawer.game.GolfGame
import com.spacebeawer.game.assets.Assets
import com.spacebeawer.game.util.LevelActor
import com.spacebeawer.game.util.SavedData

class LevelsScreen(private val game: Game): ScreenAdapter() {
    private val stage = Stage()

    init {
        createMenu()
    }

    override fun show() {
        Gdx.input.inputProcessor = stage
    }

    override fun render(delta: Float) {
        ScreenUtils.clear(0.0f, 0.0f, 0.0f, 1.0f)
        stage.act(Gdx.graphics.deltaTime.coerceAtMost(GolfGame.PERFECT_DT))
        stage.draw()
    }

    override fun resize(width: Int, height: Int) {
        stage.viewport.update(width, height)
    }

    override fun hide() {
        Gdx.input.inputProcessor = null
        stage.clear()
    }

    override fun dispose() {
        stage.dispose()
    }

    private fun createMenu() {
        val skin = Assets.skin()
        val table = Table()
        table.background = TiledDrawable(Assets.skin().getSprite("grass"))
        table.setFillParent(true)
        stage.addActor(table)
        table.pad(10.0f)

        val size = stage.width * 0.09f

        val backButton = ImageButton(SpriteDrawable(skin.getSprite("btn_left").apply { setSize(size, size) })).apply { addListener( object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    game.screen = MenuScreen(game)
                }
            })
        }
        table.top().add(backButton).padLeft(stage.width / 4.0f - size - (stage.width / 4.0f) * 0.3f).padTop(stage.height / 18).top().right().maxHeight(stage.height / 8.0f)

        table.top().add(Container(Label("Выберите уровень", skin)).apply {
            isTransform = true
            scaleBy(1.3f) }).center().expandX().maxHeight(stage.height / 8.0f)
        table.add().width(stage.width / 4.0f).expandX().maxHeight(stage.height / 8.0f)
        table.row()

        val recordsGroup = VerticalGroup()
        val scroll = ScrollPane(recordsGroup)
        scroll.setFlickScroll(true)
        recordsGroup.space(10.0f)

        table.add(scroll).top().colspan(3).padTop(10.0f).expand().fill()

        val levels = SavedData.getLevels()
        for(n in levels.indices step 2) {
            val levLineTable = Table()
            levLineTable.width = stage.width / 2.0f
            val width = stage.width * 0.184f

            levLineTable.add(LevelActor(levels[n], width, width * 0.75f).apply { addListener ( object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    game.screen = GameScreen(game, levels[n])
                }
            }) }).expand()
                .padRight(stage.width / 4.0f * 0.1f)
                .minHeight(width * 0.75f)
                .minWidth(width)

            if(n + 1 < levels.size) {
                levLineTable.add(LevelActor(levels[n+1], width, width * 0.75f).apply { addListener ( object : ClickListener() {
                    override fun clicked(event: InputEvent?, x: Float, y: Float) {
                        game.screen = GameScreen(game, levels[n + 1])
                    }
                }) }).expand().padRight(stage.width / 4.0f * 0.1f)
                    .minHeight(width * 0.75f)
                    .minWidth(width)
            }
            recordsGroup.addActor(levLineTable)
        }
        table.row()

        val clearProgressButton = TextButton("очистить прогресс",skin,"menuExit")
        clearProgressButton.addListener( object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                SavedData.clearProgress()
                stage.clear()
                createMenu()
            }
        })
        table.add(clearProgressButton).colspan(3).expandX().width(stage.width / 3).height(stage.height / 10).center().right().padBottom(10.0f).padRight(10.0f)
        clearProgressButton.labelCell.padTop(10.0f).padBottom(20.0f)
    }
}