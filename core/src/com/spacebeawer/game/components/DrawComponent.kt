package com.spacebeawer.game.components

import com.badlogic.ashley.core.Component
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.scenes.scene2d.utils.TiledDrawable

class DrawComponent(): Component {
    var textureRegion : TextureRegion? = null
    var tiledDrawable : TiledDrawable? = null
    var tintColor: Color? = null

    constructor(textureRegion: TextureRegion) : this() {
        this.textureRegion = textureRegion
    }

    constructor(drawable: TiledDrawable) : this() {
        this.tiledDrawable = drawable
    }
}