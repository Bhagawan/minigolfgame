package com.spacebeawer.game.components

import com.badlogic.ashley.core.Component
import com.badlogic.gdx.math.Vector3

class TransformComponent(x: Float, y: Float, var width: Float, var height: Float) : Component {
    val pos = Vector3(0.0f, 0.0f, 1.0f)
    var angle = 0.0f
    var scale = 1.0f
    var originX = width / 2.0f
    var originY = height / 2.0f

    init {
        pos.x = x
        pos.y = y
    }

    constructor(x: Float, y: Float, z:Float, width: Float, height: Float) : this(x, y, width, height) {
        pos.z = z
    }
}