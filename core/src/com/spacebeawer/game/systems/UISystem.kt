package com.spacebeawer.game.systems

import com.badlogic.ashley.core.EntitySystem
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable
import com.badlogic.gdx.utils.Align
import com.spacebeawer.game.assets.Assets
import com.spacebeawer.game.util.Level
import com.spacebeawer.game.util.SavedData
import com.spacebeawer.game.util.StarsActor

class UISystem(private val controlSystem: ControlSystem, priority: Int): EntitySystem(priority) {
    private val stage = Stage()
    private var currHits = 0
    private var hits = Label("0", Assets.skin()).apply { setAlignment(Align.center)  }
    private val menuDialog = createMenuDialog()

    private val nextLevel = getNextLevel(controlSystem.getLevel())
        private var uiInterface: UIInterface? = null

    init {
        createUI()
        controlSystem.setInterface(object : ControlSystem.ControlInterface {
            override fun onHit() {
                currHits++
                hits.setText(currHits)
            }

            override fun onEnd() {
                SavedData.saveRecord(controlSystem.getLevel(), currHits)
                showEndDialog()
            }
        })
    }

    override fun update(deltaTime: Float) {
        stage.act(deltaTime)
        stage.draw()
    }

    //// Public

    fun setInterface(i: UIInterface) {
        uiInterface = i
    }

    fun getStage(): Stage = stage

    //// Private

    private fun createUI() {
        val skin = Assets.skin()
        val table = Table()
        table.setFillParent(true)
        stage.addActor(table)
        table.top()

        val size = Gdx.graphics.height / 10.0f

        val pauseButton = ImageButton(skin.getDrawable("btn_menu")).apply {
            addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    controlSystem.pause(true)
                    menuDialog.show(stage)
                }
            })
        }
        table.add(pauseButton).left().top().size(size * 1.3f, size * 1.3f)

        val score = Container(hits).apply {
            background = skin.getDrawable("score_ribbon")
            pad(this@UISystem.stage.height * 0.01f,this@UISystem.stage.width * 0.04f, this@UISystem.stage.height * 0.06f,this@UISystem.stage.width * 0.04f)
            size(this@UISystem.stage.width * 0.2f, this@UISystem.stage.height * 0.1f)
        }
        table.add(score).top().expandX().height(size).padLeft(20.0f).padRight(20.0f).width(stage.width * 0.2f)

        val restartButton = ImageButton(skin.getDrawable("btn_restart")).apply {
            addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    controlSystem.restart()
                    currHits = 0
                    hits.setText(currHits)
                }
            })
        }
        table.add(restartButton).right().top().size(size * 1.3f, size * 1.3f)
    }

    private fun createMenuDialog() : Dialog {
        val skin = Assets.skin()
        val menu = Dialog("", skin)
        menu.setSize(stage.width / 3.0f, stage.height / 3.0f)
        menu.background = SpriteDrawable(skin.getSprite("popup_back").apply { setSize(stage.width / 3.0f, stage.height / 3.0f) })
        menu.contentTable.pad(50.0f)
        menu.contentTable.add(Label("Пауза", skin)).center().expand().colspan(2)
        menu.contentTable.row()

        val exitButton = ImageButton(SpriteDrawable(skin.getSprite("btn_home").apply { setSize(Gdx.graphics.height / 6.0f, Gdx.graphics.height / 6.0f) })).also {
            it.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    uiInterface?.exit()
                }
            })
        }
        menu.contentTable.add(exitButton).center().bottom().expandX().size(Gdx.graphics.height / 8.0f, Gdx.graphics.height / 8.0f).pad(20.0f)

        val continueButton = ImageButton(SpriteDrawable(skin.getSprite("btn_play").apply { setSize(Gdx.graphics.height / 6.0f, Gdx.graphics.height / 6.0f) })).also {
            it.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    menu.hide()
                    controlSystem.pause(false)
                }
            })
        }
        menu.contentTable.add(continueButton).center().bottom().expandX().size(Gdx.graphics.height / 8.0f, Gdx.graphics.height / 8.0f).pad(20.0f)

        return menu
    }

    private fun showEndDialog() {
        val skin = Assets.skin()
        val endDialog = Dialog("", skin)
        endDialog.contentTable.pad(20.0f)
        endDialog.background = SpriteDrawable(skin.getSprite("popup_back").apply { setSize(endDialog.contentTable.width * 1.2f, endDialog.contentTable.height * 1.2f) })
        endDialog.contentTable.add(Label("Мяч в лунке!", skin)).expandX().center().colspan(if(nextLevel == null) 2 else 3)
        endDialog.contentTable.row()

        val size = Gdx.graphics.height / 9.0f

        val level = controlSystem.getLevel()
        val result = StarsActor(if(currHits <= level.perfectScore ) 3
            else if(currHits <= level.perfectScore + 2) 2
            else 1, size * 4, size)
        endDialog.contentTable.add(result).colspan(if(nextLevel == null) 2 else 3).size(size * 4, size).expandX()
        endDialog.contentTable.row()

        val exitButton = ImageButton(SpriteDrawable(skin.getSprite("btn_home").apply { setSize(size, size) })).also {
            it.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    uiInterface?.exit()
                }
            })
        }
        endDialog.contentTable.add(exitButton).center().bottom().expandX().size(stage.width / 15.0f, stage.width / 15.0f)

        val restartButton = ImageButton(SpriteDrawable(skin.getSprite("btn_restart").apply { setSize(size, size) })).also {
            it.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    controlSystem.restart()
                    currHits = 0
                    hits.setText(currHits)
                    endDialog.hide()
                }
            })
        }
        endDialog.contentTable.add(restartButton).center().bottom().expandX().size(stage.width / 15.0f, stage.width / 15.0f)

        if(nextLevel != null) {
            val nextLevelButton = ImageButton(SpriteDrawable(skin.getSprite("btn_next").apply { setSize(size, size) })).also {
                it.addListener(object : ClickListener() {
                    override fun clicked(event: InputEvent?, x: Float, y: Float) {
                        nextLevel.let {level -> uiInterface?.nextLevel(level) }
                        endDialog.hide()
                    }
                })
            }
            endDialog.contentTable.add(nextLevelButton).center().bottom().expandX().size(stage.width / 15.0f, stage.width / 15.0f)
        }

        endDialog.show(stage)
    }

    private fun getNextLevel(level: Level): Level? {
        val levels = SavedData.getLevels()
        for(n in levels.indices) {
            if(levels[n] == level && n < levels.size - 1) {
                return levels[n + 1]
            }
        }
        return null
    }

    interface UIInterface {
        fun exit()
        fun nextLevel(level: Level)
    }
}