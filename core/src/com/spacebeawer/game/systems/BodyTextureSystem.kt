package com.spacebeawer.game.systems

import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.EntitySystem
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.physics.box2d.Body
import com.badlogic.gdx.physics.box2d.World
import com.badlogic.gdx.utils.Array
import com.spacebeawer.game.components.TransformComponent

class BodyTextureSystem(private val world :World, priority: Int) : EntitySystem(priority) {
    private val one = Gdx.graphics.height / 20.0f

    override fun update(deltaTime: Float) {
        val bodies : Array<Body> = Array()
        world.getBodies(bodies)
        for(body in bodies) {
            val entity = body.userData
            if(entity is Entity) {
                entity.getComponent(TransformComponent::class.java)?.let {
                    it.pos.x = body.position.x * one
                    it.pos.y = body.position.y * one
                    it.angle = 180 + MathUtils.radiansToDegrees * body.angle
                }
            }
        }
    }
}