package com.spacebeawer.game.systems

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.EntitySystem
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.InputAdapter
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.*
import com.spacebeawer.game.assets.Assets
import com.spacebeawer.game.components.DrawComponent
import com.spacebeawer.game.components.TransformComponent
import com.spacebeawer.game.util.Coordinate
import com.spacebeawer.game.util.Level
import java.lang.Float.min
import kotlin.math.acos
import kotlin.math.pow
import kotlin.math.sqrt
import kotlin.random.Random

class ControlSystem(private val world: World, private val camera: Camera, private val level: Level, priority: Int): EntitySystem(priority) {
    private val one = Gdx.graphics.height / 20.0f
    private var arrowHead: Entity = Entity().apply {
        add(DrawComponent(Assets.skin().getSprite("arrow_head")))
        add(TransformComponent(0.0f,0.0f,one,0.0f).apply { originY = 0.0f })
    }
    private var arrowBody: Entity = Entity().apply {
        add(DrawComponent(Assets.atlas().findRegion("white-pixel")))
        add(TransformComponent(0.0f,0.0f,one / 3.0f,0.0f).apply { originY = 0.0f })
    }
    private var ballTC: TransformComponent? = null
    private var ball: Body? = null
    private var hole = Coordinate(0.0f,0.0f)

    companion object {
        const val STATE_AIM = 0
        const val STATE_CAMERA = 1
        const val STATE_INTRO = 2
        const val STATE_PAUSE = 3
    }
    private var pausedState = STATE_CAMERA

    private var mInterface : ControlInterface? = null
    private var state = STATE_INTRO
    private var introSpeed = Gdx.graphics.width / 30.0f

    private val inputAdapter = object: InputAdapter() {
        override fun touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
            when(state) {
                STATE_AIM -> {
                    engine.addEntity(arrowHead)
                    engine.addEntity(arrowBody)
                    updateArrow(screenX.toFloat(), screenY.toFloat())
                    return true
                }
            }
            return false
        }

        override fun touchUp(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
            when(state) {
                STATE_AIM -> {
                    updateArrow(screenX.toFloat(), screenY.toFloat())
                    hitBall(screenX.toFloat(), screenY.toFloat())
                    return true
                }
                STATE_INTRO -> introSpeed *= 10
                STATE_CAMERA -> {
                    ball?.let {
                        if(it.linearVelocity.len() < 1.0f) it.linearVelocity = Vector2(0.0f,0.0f)
                    }
                }
            }
            return false
        }

        override fun touchDragged(screenX: Int, screenY: Int, pointer: Int): Boolean {
            when(state) {
                STATE_AIM -> {
                    updateArrow(screenX.toFloat(), screenY.toFloat())
                    return true
                }
            }
            return false
        }
    }

    override fun addedToEngine(engine: Engine?) {
        createLevel()
    }

    override fun update(deltaTime: Float) {
        updateCamera()
        checkBall()
    }

    //// Public ///

    fun setInterface(i: ControlInterface) {
        mInterface = i
    }

    fun getLevel(): Level = level

    fun pause(pause: Boolean) {
        if(pause) {
            pausedState = state
            state = STATE_PAUSE
        } else state = pausedState
    }

    fun restart() {
        state = STATE_AIM
        ball?.userData?.let {
            if(it is Entity)engine.removeEntity(it)
        }
        world.destroyBody(ball)
        createBoll()
    }

    /// Private ///

    fun getInputAdapter(): InputAdapter = inputAdapter

    private fun createLevel() {
        createBorder()
        createObstacles()
        createBoll()
        createHole()
    }

    private fun createBorder() {
        if(level.border.size > 1) {
            for(n in 1 until level.border.size) {
                val pVN = level.border[n-1]
                val vN = level.border[n]
                val stumpBodyDef = BodyDef()
                stumpBodyDef.position.set(level.vertices[pVN].x, level.vertices[pVN].y)
                stumpBodyDef.angle = Random.nextFloat() * Math.PI.toFloat()
                val stumpBody = world.createBody(stumpBodyDef)
                val stumpShape = CircleShape()
                stumpShape.radius = 0.5f
                stumpBody.createFixture(stumpShape, 0.0f)
                stumpShape.dispose()
                val stumpEntity = Entity()
                val stumpTC = TransformComponent(level.vertices[pVN].x * one, level.vertices[pVN].y * one, one, one)
                stumpTC.pos.z = 2.0f
                stumpEntity.add(stumpTC)
                stumpEntity.add(DrawComponent(Assets.atlas().findRegion("stump")))
                engine.addEntity(stumpEntity)
                stumpBody.userData = stumpEntity

                val wallV = Coordinate(level.vertices[vN].x - level.vertices[pVN].x, level.vertices[vN].y - level.vertices[pVN].y)
                val length = sqrt(wallV.x.pow(2) + wallV.y.pow(2))
                var angle = Math.toDegrees(acos((wallV.x) / (length)).toDouble()).toFloat()
                if(level.vertices[vN].y >= level.vertices[pVN].y) angle += 90
                else angle = 90 - angle

                val bodyDef = BodyDef()
                bodyDef.position.set(level.vertices[pVN].x, level.vertices[pVN].y)
                bodyDef.angle = Math.toRadians(angle.toDouble()).toFloat()
                val wallBody = world.createBody(bodyDef)

                val shape = PolygonShape()
                shape.setAsBox(0.5f, length / 2, Vector2(0.0f, -length / 2.0f), 0.0f)
                wallBody.createFixture(shape, 0.0f)
                shape.dispose()

                val entity = Entity()
                val tC = TransformComponent((level.vertices[pVN].x - 0.5f) * one, (level.vertices[pVN].y - 0.5f) * one, one, length * one)
                tC.originY = 0.0f
                tC.angle = 180 + angle
                entity.add(tC)
                entity.add(DrawComponent(Assets.atlas().findRegion("plank")))
                engine.addEntity(entity)
                wallBody.userData = entity
            }
        }
    }

    private fun createObstacles() {
        for(o in level.obstacles) {
            if(o.line.size > 1) {
                for(n in 1 until o.line.size) {
                    val pVN = o.line[n-1]
                    val vN = o.line[n]
                    val stumpBodyDef = BodyDef()
                    stumpBodyDef.position.set(level.vertices[pVN].x, level.vertices[pVN].y)
                    val stumpBody = world.createBody(stumpBodyDef)
                    val stumpShape = CircleShape()
                    stumpShape.radius = 0.5f
                    stumpBody.createFixture(stumpShape, 0.0f)
                    stumpShape.dispose()
                    val stumpEntity = Entity()
                    val stumpTC = TransformComponent(level.vertices[pVN].x * one, level.vertices[pVN].y * one, one, one)
                    stumpTC.pos.z = 2.0f
                    stumpTC.angle = Random.nextInt(360).toFloat()
                    stumpEntity.add(stumpTC)
                    stumpEntity.add(DrawComponent(Assets.atlas().findRegion("stump")))
                    engine.addEntity(stumpEntity)
                    stumpBody.userData = stumpEntity

                    val wallV = Coordinate(level.vertices[vN].x - level.vertices[pVN].x, level.vertices[vN].y - level.vertices[pVN].y)
                    val length = sqrt(wallV.x.pow(2) + wallV.y.pow(2))
                    var angle = Math.toDegrees(acos((wallV.x) / (length)).toDouble()).toFloat()
                    if(level.vertices[vN].y >= level.vertices[pVN].y) angle += 90
                    else angle = 90 - angle

                    val bodyDef = BodyDef()
                    bodyDef.position.set(level.vertices[pVN].x, level.vertices[pVN].y)
                    bodyDef.angle = Math.toRadians(angle.toDouble()).toFloat()
                    val wallBody = world.createBody(bodyDef)

                    val shape = PolygonShape()
                    shape.setAsBox(0.5f, length / 2, Vector2(0.0f, -length / 2.0f), 0.0f)
                    wallBody.createFixture(shape, 0.0f)
                    shape.dispose()

                    val entity = Entity()
                    val tC = TransformComponent(level.vertices[pVN].x * one, level.vertices[pVN].y * one, one, length * one)
                    tC.originY = 0.0f
                    tC.angle = 180 + angle
                    entity.add(tC)
                    entity.add(DrawComponent(Assets.atlas().findRegion("plank")))
                    engine.addEntity(entity)
                    wallBody.userData = entity
                }

                val stumpBodyDef = BodyDef()
                stumpBodyDef.position.set(level.vertices[o.line.last()].x, level.vertices[o.line.last()].y)
                val stumpBody = world.createBody(stumpBodyDef)
                val stumpShape = CircleShape()
                stumpShape.radius = 0.5f
                stumpBody.createFixture(stumpShape, 0.0f)
                stumpShape.dispose()
                val stumpEntity = Entity()
                val stumpTC = TransformComponent(level.vertices[o.line.last()].x * one - one / 2.0f, level.vertices[o.line.last()].y * one - one / 2.0f, one, one)
                stumpTC.pos.z = 2.0f
                stumpTC.angle = Random.nextInt(360).toFloat()
                stumpEntity.add(stumpTC)
                stumpEntity.add(DrawComponent(Assets.atlas().findRegion("stump")))
                engine.addEntity(stumpEntity)
                stumpBody.userData = stumpEntity
            }
        }
    }

    private fun createHole() {
        val hole = Entity()
        hole.add(TransformComponent(level.hole.x * one, level.hole.y * one, one, one).apply { pos.z = 0.5f })
        hole.add(DrawComponent(Assets.atlas().findRegion("hole")))
        engine.addEntity(hole)
        this.hole = level.hole
        if(state == STATE_INTRO) {
            camera.translate(level.hole.x * one - camera.position.x, level.hole.y * one - camera.position.y, 0.0f)
            camera.update()
        }
    }

    private fun createBoll() {
        val bodyDef = BodyDef()
        bodyDef.position.set(0.0f, 0.0f)
        bodyDef.type = BodyDef.BodyType.DynamicBody
        ball = world.createBody(bodyDef)

        val fixDef = FixtureDef()
        val shape = CircleShape()
        shape.radius = 0.5f
        fixDef.shape = shape
        fixDef.density = 0.5f
        fixDef.friction = 0.5f
        fixDef.restitution = 0.5f

        ball!!.createFixture(fixDef)
        shape.dispose()

        ball!!.linearDamping = 0.3f
        ball!!.angularDamping = 3.5f

        val entity = Entity()
        ballTC = TransformComponent(0.0f,0.0f, one, one)
        ballTC!!.angle = Random.nextInt(360).toFloat()
        entity.add(ballTC)
        entity.add(DrawComponent(Assets.atlas().findRegion("ball")))
        engine.addEntity(entity)
        ball!!.userData = entity
    }

    private fun updateArrow(touchX: Float, touchY: Float) {
        ballTC?.let { bTC ->
            val ballX = Gdx.graphics.width / 2.0f
            val ballY = Gdx.graphics.height / 2.0f
            val arrowV = Vector2( ballX - touchX, ballY - Gdx.graphics.height + touchY)
            val length = arrowV.len()
            val angle = 90 - arrowV.angleDeg()

            val perc = if(length - bTC.height / 2.0f  <= bTC.width) {
                (bTC.height / 2) / length
            } else (length - bTC.height ) / length

            val headStartX = bTC.pos.x + arrowV.x * perc
            val headStartY = bTC.pos.y + arrowV.y * perc
            val bodyStartX = bTC.pos.x + arrowV.x * ((bTC.height / 2) / length)
            val bodyStartY = bTC.pos.y + arrowV.y * ((bTC.height / 2) / length)

            val headTC = arrowHead.getComponent(TransformComponent::class.java)
            headTC?.let {
                it.pos.x = headStartX
                it.pos.y = headStartY
                it.height = min(bTC.width, length - bTC.width / 2)
                it.angle = -angle
            }
            val bodyTC = arrowBody.getComponent(TransformComponent::class.java)
            bodyTC?.let {
                it.pos.x = bodyStartX
                it.pos.y = bodyStartY
                it.height = (length - bTC.width * 1.5f).coerceAtLeast(0.0f)
                it.angle = -angle
            }
        }
    }

    private fun hitBall(touchX: Float, touchY: Float) {
        engine.removeEntity(arrowHead)
        engine.removeEntity(arrowBody)
        ballTC?.let {
            val dSpeed = one / 3
            ball?.linearVelocity = Vector2((Gdx.graphics.width / 2.0f - touchX) / dSpeed, (touchY - Gdx.graphics.height / 2.0f ) / dSpeed)
        }
        state = STATE_CAMERA
        mInterface?.onHit()
    }

    private fun updateCamera() {
        when(state) {
            STATE_AIM -> ball?.let {
                camera.position.set(it.position.x * one, it.position.y * one, 0.0f)
            }
            STATE_CAMERA -> {
                ball?.let {
                    camera.position.set(it.position.x * one, it.position.y * one, 0.0f)
                    if(it.linearVelocity.isZero) state = STATE_AIM
                }
            }
            STATE_INTRO -> {
                ball?.let {
                    val v = Vector2(it.position.x - camera.position.x, it.position.y - camera.position.y)
                    if(v.len() <= introSpeed) {
                        camera.position.set(it.position.x * one, it.position.y * one, 0.0f)
                        state = STATE_AIM
                    } else {
                        val nor = v.nor().scl(introSpeed)
                        camera.translate(nor.x, nor.y, 0.0f)
                    }
                }
            }
        }
        camera.update()
    }

    private fun checkBall() {
        ball?.let {
            if(it.position?.epsilonEquals(hole.x, hole.y, 1.0f) == true && state != STATE_PAUSE) {
                it.linearVelocity = Vector2((hole.x - it.position.x) * 5, (hole.y - it.position.y) * 5)
                if(it.position?.epsilonEquals(hole.x, hole.y, 0.2f) == true){
                    ballTC?.let { tc ->
                        tc.width *= 0.95f
                        tc.height *= 0.95f
                        tc.originX = tc.width / 2.0f
                        tc.originY = tc.height / 2.0f
                        if(tc.width <= one / 10) {
                            it.linearVelocity = Vector2(0.0f,0.0f)
                            state = STATE_PAUSE
                            mInterface?.onEnd()
                        }
                    }
                }
            }
        }
    }

    interface ControlInterface {
        fun onHit()
        fun onEnd()
    }
}