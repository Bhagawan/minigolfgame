package com.spacebeawer.game.systems

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.EntitySystem
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.scenes.scene2d.utils.TiledDrawable
import com.spacebeawer.game.assets.Assets
import com.spacebeawer.game.components.DrawComponent
import com.spacebeawer.game.components.TransformComponent

class BackgroundSystem(private val camera: Camera, priority: Int): EntitySystem(priority) {
    private val one = Gdx.graphics.height / 20.0f

    private var back = Entity().apply {
        val tR = Assets.skin().getSprite("grass")
        add(TransformComponent(-tR.width,-tR.height, Gdx.graphics.width.toFloat() + one * 2, Gdx.graphics.height.toFloat() + one * 2).apply { pos.z = 0.0f })
        add(DrawComponent(TiledDrawable(tR).apply { scale = one / tR.regionHeight }))
    }

    override fun addedToEngine(engine: Engine?) {
        engine?.addEntity(back)
    }

    override fun update(deltaTime: Float) {
        back.getComponent(TransformComponent::class.java)?.pos?.set((camera.position.x - (camera.position.x % one)), (camera.position.y - (camera.position.y % one)), 0.0f)
    }
}