package com.spacebeawer.game.util

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.Actor
import com.spacebeawer.game.assets.Assets

class StarsActor(private val golden: Int, private var w: Float, private val h: Float,private val animationTime: Float = 1.0f): Actor() {
    private val starGrey = Assets.skin().getSprite("star_grey")
    private val starGold = Assets.skin().getSprite("star_gold")
    private var currAnimTime = 0.0f

    private val starH = h * 0.9f

    override fun setSize(width: Float, height: Float) {
        super.setSize(width, height)
        w = width
    }

    override fun draw(batch: Batch?, parentAlpha: Float) {
        batch?.let {
            for(n in 0..2) {
                val size : Float = (starH * (if(animationTime > 0) animationTime - animationTime / 3.0f * n else 1.0f) / (animationTime / 3.0f)).coerceIn(0.0f, starH)
                val xx = x + w * 0.5f - starH + starH * n
                val star = if(n < golden ) starGold
                else starGrey
                it.draw(star, xx - size / 2.0f,y + (h - size) / 2, size, size)
            }
        }
        if(currAnimTime < animationTime) currAnimTime++
    }
}