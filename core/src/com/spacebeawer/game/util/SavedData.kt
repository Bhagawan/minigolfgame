package com.spacebeawer.game.util

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.utils.Json

class SavedData {
    companion object {

        private fun clearLevels() {
            val prefs = Gdx.app.getPreferences("GolfData")
            val j = Json()
            prefs.putString("levels", j.toJson(emptyArray<Level>()))
            prefs.flush()
        }

        fun updateLevels(newList: List<Level>) {
            val prefs = Gdx.app.getPreferences("GolfData")
            val j = Json()
            var levels = j.fromJson(Array<Level>::class.java, prefs.getString("levels", "[]"))
            for(newLevel in newList) if(!levels.contains(newLevel)) levels = levels.plusElement(newLevel)
            prefs.putString("levels", j.toJson(levels))
            prefs.flush()
        }

        fun saveRecord(level: Level, record: Int) {
            val prefs = Gdx.app.getPreferences("GolfData")
            val j = Json()
            val levels = j.fromJson(Array<Level>::class.java, prefs.getString("levels", "[]"))
            for(l in levels) if(level == l) {
                if(l.lastResult != null) {
                    if(l.lastResult!! > record) l.lastResult = record
                } else l.lastResult = record
            }
            prefs.putString("levels", j.toJson(levels))
            prefs.flush()
        }

        fun getLevels(): Array<Level> {
            val prefs = Gdx.app.getPreferences("GolfData")
            val r = prefs.getString("levels", "[]")
            return Json().fromJson(Array<Level>::class.java, r)
        }

        fun clearProgress() {
            val prefs = Gdx.app.getPreferences("GolfData")
            val j = Json()
            val levels = j.fromJson(Array<Level>::class.java, prefs.getString("levels", "[]"))
            for(level in levels) level.lastResult = null
            prefs.putString("levels", j.toJson(levels))
            prefs.flush()
        }
    }
}