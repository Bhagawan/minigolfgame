package com.spacebeawer.game.util

data class Coordinate(var x: Float = 0.0f, var y: Float = 0.0f)