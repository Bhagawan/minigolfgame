package com.spacebeawer.game.util

data class Level(val name: String = "",
                 var vertices : List<Coordinate> = emptyList(),
                 val border: List<Int> = emptyList(),
                 var obstacles: List<Obstacle> = emptyList(),
                 val hole: Coordinate = Coordinate(0.0f,0.0f),
                 val perfectScore : Int = 1,
                 var lastResult: Int? = null) {

    override fun equals(other: Any?): Boolean {
        return if(other is Level) name == other.name && vertices == other.vertices && obstacles == other.obstacles && hole == other.hole
        else false
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + vertices.hashCode()
        result = 31 * result + border.hashCode()
        result = 31 * result + obstacles.hashCode()
        result = 31 * result + hole.hashCode()
        return result
    }
}
