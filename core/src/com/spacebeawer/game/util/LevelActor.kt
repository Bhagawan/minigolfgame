package com.spacebeawer.game.util

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.utils.Align
import com.spacebeawer.game.assets.Assets

class LevelActor(private val level: Level, private val w: Float, private val h: Float): Actor() {
    private val back = Assets.skin().getSprite("popup_back")
    private val stars = StarsActor(
        if(level.lastResult == null) 0
        else {
            if(level.lastResult!! <= level.perfectScore ) 3
            else if(level.lastResult!! <= level.perfectScore + 2) 2
            else 1
        }, w * 0.8f, h / 3.0f).apply {
        setSize(w * 0.8f, h / 3.0f)
        setPosition(x + w * 0.1f, y + h * 0.1f)
    }

    override fun draw(batch: Batch?, parentAlpha: Float) {

        stars.setPosition(x + w * 0.1f, y + h * 0.1f)
        batch?.let {
            it.draw(back, x, y, w, h)
            Assets.skin().getFont("Italic").apply {
                color = Color.BLUE
            }.draw(it, level.name, x + w * 0.1f,y + h / 3.0f * 2.4f, w * 0.8f, Align.center, true)

            stars.draw(batch, parentAlpha)
        }

    }
}