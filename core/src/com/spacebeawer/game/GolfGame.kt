package com.spacebeawer.game

import com.badlogic.gdx.Game
import com.spacebeawer.game.assets.Assets
import com.spacebeawer.game.screens.MenuScreen
import com.spacebeawer.game.util.Level
import com.spacebeawer.game.util.SavedData

class GolfGame(private val levels: List<Level>): Game() {

	companion object {
		const val PERFECT_DT:Float=(16.67*2/1000).toFloat()
	}

	override fun create(){
		Assets.init()
		SavedData.updateLevels(levels)
		setScreen(MenuScreen(this))
	}

	override fun dispose(){
		Assets.dispose()
	}
}
